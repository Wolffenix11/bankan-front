export class Card {
    public id: number
    public title: string
    public description: string
    public listId: number
}

export class RegisterCard {
    public title: string
    public description: string
    public listId: number

    constructor(title: string,
                description: string,
                listId: number) {

        this.title = title;
        this.description = description;
        this.listId = listId;
    }
}