import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Card, RegisterCard } from 'src/app/model/card.model';
import { Lane } from 'src/app/model/lane.model';
import { CardService } from 'src/app/service/card.service';
import { LaneService } from 'src/app/service/lane.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { filter } from 'rxjs/operators';
import { SessionService } from 'src/app/service/session.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  lanes: Lane[] = [];
  cards: Card[] = [];
  add_to_lane: number = 0;
  edit_card: Card = null;

  formAddCard = this.formBuilder.group({
    title: ['', [Validators.required, Validators.maxLength(50)]],
    description: ['', [Validators.required]],
  });

  formEditCard = this.formBuilder.group({
    title: ['', [Validators.required, Validators.maxLength(50)]],
    description: ['', [Validators.required]],
  });

  constructor(
    private laneService: LaneService,
    private cardService: CardService,
    private formBuilder: FormBuilder,
    private sessionService: SessionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.findLanes();
    this.findCards();
  }

  findLanes() {
    this.laneService.findAll().subscribe(
      response => this.lanes = response,
      err => {
        if (err.status === 403) {
          this.logout();
        }
      }
    );
  }

  findCards() {
    this.cardService.findAll().subscribe(
      response => this.cards = response
    );
  }

  openAddCardModal(laneId) {
    this.add_to_lane = laneId;
  }

  openEditCardModal(cardId) {
    this.edit_card = new Card()
    this.cardService.findById(cardId).subscribe(
      response => {
        this.edit_card = response;
        this.formEditCard.controls.title.setValue(this.edit_card.title);
        this.formEditCard.controls.description.setValue(this.edit_card.description);          
        console.log(this.edit_card);
      }
    );
  }

  closeAddCardModal() {
    this.add_to_lane = -1;
    this.formAddCard.reset();
  }

  closeEditModal() {
    this.edit_card = null;
    this.formAddCard.reset();
  }

  createCard() {
    let card = new RegisterCard(
      this.addCardtitle.value,
      this.addCardDescription.value,
      this.add_to_lane
    )
    this.cardService.save(card).subscribe(
      response => {
        this.closeAddCardModal();
        this.findCards();
      }
    );
  }

  updateCard() {
    this.edit_card.title = this.formEditCard.controls.title.value;
    this.edit_card.description = this.formEditCard.controls.description.value;
    this.cardService.findById(this.edit_card.id).subscribe(
      response => {
        response["title"] = this.edit_card.title;
        response["description"] = this.edit_card.description;
        this.cardService.update(response).subscribe();
        this.closeEditModal();
        this.findCards();
      }
    )
  }

  logout() {
    this.sessionService.logout();
    this.router.navigateByUrl("/login");
  }

  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    let data = ev.dataTransfer.getData("text");
    let targetLane = this.getTargetLane(ev.path);

    if (targetLane && data) {
      targetLane.appendChild(document.getElementById(data));
      
      let cardId = data.split("-")[1];
      let laneId = targetLane.id.split("-")[1]
      this.changeCardLane(cardId, laneId);
    }
  }

  private getTargetLane(path) {
    return path.filter(element => element.id ? element.id.includes("lane") : false)[0];
  }

  private changeCardLane(cardId, laneId) {
    this.cardService.findById(cardId).subscribe(
      response => {
        response["listId"] = laneId
        this.cardService.update(response).subscribe();
      }
    );

  }

  get addCardtitle() {
    return this.formAddCard.controls.title;
  }

  get addCardDescription() {
    return this.formAddCard.controls.description;
  }

}
