const URL_BASE = "http://localhost:8080";

export const WS_LANES = URL_BASE + "/lanes/";

export const WS_CARDS = URL_BASE + "/cards/";

export const WS_LOGIN = URL_BASE + "/login";