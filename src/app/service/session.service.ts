import { Injectable } from "@angular/core";
import { AuthResult } from "../model/login.model";
import { User } from "../model/user.model";

const AUTH_DATA =  "bankan-auth-data"

const ROLE_ADMIN = "ROLE_ADMIN"
const ROLE_CLIENT = "ROLE_CLIENT"

@Injectable({
    providedIn: 'root'
})
export class SessionService {

    private mUser?: User
    get user() {
        return this.mUser
    }

    private mAuthToken?: string
    get authToken() {
        return this.mAuthToken
    }

    get isLoggedIn() {
        return this.mUser != null
    }

    get isAdmin() {
        return this.isUserInRole(ROLE_ADMIN)
    }

    get isClient() {
        return this.isUserInRole(ROLE_CLIENT)
    }

    constructor() {
        this.loadAuthData()
    }

    authenticate(authResult: AuthResult) {
        this.cacheAuthData(authResult)

        sessionStorage.setItem(AUTH_DATA, JSON.stringify(authResult))
    }

    logout() {
        this.mUser = undefined
        this.mAuthToken = undefined
        sessionStorage.removeItem(AUTH_DATA)
    }

    private cacheAuthData(authResult: AuthResult) {
        this.mUser = authResult.userData
        this.mAuthToken = authResult.token
    }

    private loadAuthData() {
        let authDataString = sessionStorage.getItem(AUTH_DATA)
        
        if (authDataString != null) {
            let authResult = JSON.parse(authDataString) as AuthResult
            this.cacheAuthData(authResult)
        }
    }

    private isUserInRole(role: string): Boolean {
        let index = this.mUser?.roles.indexOf(role)
        return (index != null && index != -1)
    }

}