import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Lane } from '../model/lane.model';
import { DefaultResponse } from '../model/response.model';
import { WS_LANES } from './endpoints';

@Injectable({
  providedIn: 'root'
})
export class LaneService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Lane[]> {
    return this.http.get<DefaultResponse<Lane[]>>(WS_LANES).pipe(
      map(response => { console.log(response); return response.result})
    )
  }
}
