import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DefaultResponse } from '../model/response.model';
import { WS_CARDS } from './endpoints';
import { Card, RegisterCard } from '../model/card.model';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Card[]> {
    return this.http.get<DefaultResponse<Card[]>>(WS_CARDS).pipe(
      map(response => response.result)
    )
  }

  findById(id: number): Observable<Card> {
    return this.http.get<DefaultResponse<Card>>(WS_CARDS + id).pipe(
      map(response => response.result)
    )
  }

  save(card: RegisterCard): Observable<boolean> {
    return this.http.post<DefaultResponse<boolean>>(WS_CARDS, card).pipe(
      map(response => response.result)
    )
  }

  update(card: Card): Observable<boolean> {
    return this.http.put<DefaultResponse<boolean>>(WS_CARDS, card).pipe(
      map(response => response.result)
    )
  }
}
