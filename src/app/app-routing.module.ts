import { NgModule } from '@angular/core';
import { NavigationStart, Router, RouterModule, Routes } from '@angular/router';
import { SessionService } from './service/session.service';
import { DashboardComponent } from './ui/dashboard/dashboard.component';

import { LoginComponent } from './ui/login/login.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: '', pathMatch: 'full', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor(private router: Router,
      private sessionService: SessionService) {
      this.setupRouterListener()
  }

  private setupRouterListener() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (!this.sessionService.isLoggedIn && event.url !== "/login") {
          this.router.navigateByUrl('/login');
        } else if (this.sessionService.isLoggedIn && event.url !== "/dashboard") {
          this.router.navigateByUrl('/dashboard');
        }
      }
    })
  }
}
